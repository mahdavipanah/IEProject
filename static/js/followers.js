window.addEventListener('load', () => {
    document.querySelectorAll('.user-div button').forEach(element => {
        element.addEventListener('click', () => {
            if (element.getAttribute('class') === 'no-follow') {
                element.setAttribute('class', 'followed');
                element.textContent = "دنبال شده";
                followRequest(element.getAttribute('userId'), (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
            } else {
                element.setAttribute('class', 'no-follow');
                element.textContent = "دنبال کن";
                unfollowRequest(element.getAttribute('userId'), (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
            }
        });
    });
});
