function tweetLikeRequest (tweetId, callback) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status !== 201) {
                if (callback)
                    callback(this.status);
            } else {
                if (callback)
                    callback(null);
            }
        }
    };
    xhttp.open('POST', `/tweets/id/${tweetId}/like`);
    xhttp.send();
}

function tweetUnlikeRequest (tweetId, callback) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status !== 200) {
                if (callback)
                    callback(this.status);
            } else {
                if (callback)
                    callback(null);
            }
        }
    };
    xhttp.open('POST', `/tweets/id/${tweetId}/unlike`);
    xhttp.send();
}