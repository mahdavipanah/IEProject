function followRequest (friendId, callback) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status !== 201) {
                if (callback)
                    callback(this.status);
            } else {
                if (callback)
                    callback(null);
            }
        }
    };
    xhttp.open('POST', `/users/id/${friendId}/follow`);
    xhttp.send();
}

function unfollowRequest (friendId, callback) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status !== 200) {
                if (callback)
                    callback(this.status);
            } else {
                if (callback)
                    callback(null);
            }
        }
    };
    xhttp.open('POST', `/users/id/${friendId}/un-follow`);
    xhttp.send();
}