window.addEventListener('load', () => {
    element = document.querySelector('.follow-link-div button');

    if (element)
        element.addEventListener('click', () => {
            if (element.getAttribute('class') === 'no-follow') {
                element.setAttribute('class', 'followed');
                element.textContent = "دنبال شده";
                followRequest(element.getAttribute('userId'), (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
            } else {
                element.setAttribute('class', 'no-follow');
                element.textContent = "دنبال کن";
                unfollowRequest(element.getAttribute('userId'), (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
            }
        });

    document.querySelectorAll('.heart').forEach(element => {
        element.addEventListener('click', () => {
            const tweetId = element.getAttribute('tweetId');
            const likesCountElement = document.getElementById(`tweet${tweetId}LikesCount`);

            if (element.classList.contains('is-active')) {
                tweetUnlikeRequest(tweetId, (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
                likesCountElement.textContent = (parseInt(likesCountElement.textContent) - 1).toString();
            } else {
                tweetLikeRequest(tweetId, (err) => {
                    if (err !== null)
                        alert('مشکلی در ارتباط با سرور پیش آمده، لطفا خارج و سپس دوباره وارد شوید');
                });
                likesCountElement.textContent = (parseInt(likesCountElement.textContent) + 1).toString();
            }

            element.classList.toggle('is-active');
        });
    })
});
