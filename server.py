from os import path, remove
import string
import random
import sqlite3
import json
import re
import ntpath

import tornado.ioloop
import tornado.web
import bcrypt


db = None

def generateRandomString(length):
    s = string.ascii_lowercase + string.digits + string.ascii_uppercase
    return str(''.join(random.sample(s, length)))


def get_user_info (user_id, by='id', pre=''):
    retVal = dict()
    cur = db.cursor()

    if by == 'id':
        cur.execute('SELECT name, username, id FROM users WHERE id=?', (int(user_id),))
    else:
        cur.execute('SELECT name, username, id FROM users WHERE username=?', (user_id,))

    user = cur.fetchone()

    if not user:
        return None

    retVal[pre + 'name'] = user[0]
    retVal[pre + 'username'] = user[1]
    retVal[pre + 'user_id'] = str(user[2])

    cur.execute('SELECT count(*) FROM usersFriends WHERE userId=?', (int(retVal[pre + 'user_id']),))
    retVal[pre + 'followingCount'] = cur.fetchone()[0]

    cur.execute('SELECT count(*) FROM usersFriends WHERE friendId=?', (int(retVal[pre + 'user_id']),))
    retVal[pre + 'followersCount'] = cur.fetchone()[0]

    cur.close()
    return retVal


class LoginHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('login.html', username='', errors=[])

    def post(self):
        errors = []

        username = self.get_body_argument('username', default=None)
        password = self.get_body_argument('password', default=None)

        if not username or not len(username):
            errors.append('نام کاربری نباید خالی باشد')
            username = ''

        if not password or not len(password):
            errors.append('رمز عبور نباید خالی باشد')
        elif len(password) < 6:
            errors.append('رمز عبور باید بزرگتر از شش حرف باشد')

        user = None
        if not len(errors):
            cur = self.application.db.cursor()
            cur.execute('SELECT id, password FROM users WHERE username=?', (username,))

            user = cur.fetchone()

            cur.close()

            if not user:
                errors.append('نام کاربری یا رمز عبور اشتباه است')
            else:
                if not bcrypt.hashpw(password.encode('utf8'), user[1]) == user[1]:
                    errors.append('نام کاربری یا رمز عبور اشتباه است')

        if len(errors):
            self.render('login.html', username=username, errors=errors)
        else:
            self.set_secure_cookie('id', str(user[0]))
            self.set_secure_cookie('username', username)
            self.redirect('/@' + username)


class SignupHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('signup.html', name='', username='', email='', errors=[])

    def post(self):
        errors = []

        name = self.get_body_argument('name', default=None)
        username = self.get_body_argument('username', default=None)
        email = self.get_body_argument('email', default=None)
        avatar = self.request.files.get('avatar', None)
        password = self.get_body_argument('password', default=None)
        password_repeat = self.get_body_argument('password_repeat', default=None)

        if not name or not len(name):
            errors.append('نام نباید خالی باشد')
            name = ''

        if not username or not len(username):
            errors.append('نام کاربری نباید خالی باشد')
            username = ''

        if not email or not len(email):
            errors.append('ایمیل نباید خالی باشد')
            email = ''
        else:
            if not re.match("[^@]+@[^@]+\.[^@]+", email):
                errors.append('فرمت ایمیل نادرست است')
                email = ''

        if not password or not len(password):
            errors.append('رمز عبور نباید خالی باشد')
        elif len(password) < 6:
            errors.append('رمز عبور باید بزرگتر از شش حرف باشد')
        else:
            if not password_repeat or not len(password_repeat):
                errors.append('تکرار رمز عبور نباید خالی باشد')
            elif password_repeat != password:
                errors.append('رمز عبور و تکرار آن تطابق ندارند')

        if avatar and username != '':
            upload_file = self.request.files['avatar'][0]
            avatar_path = path.join(path.dirname(__file__), 'uploads/avatars') + '/' + username + generateRandomString(5) + upload_file['filename']
            with open(avatar_path, 'wb') as file:
                file.write(upload_file['body'])
            avatar = avatar_path

        new_user_id = None
        if not len(errors):
            try:
                cur = self.application.db.cursor()
                cur.execute(
                    'INSERT INTO users(name, username, email, avatar, password) VALUES (?, ?, ?, ?, ?)',
                    (name, username, email, avatar,
                     bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt()),)
                )
                self.application.db.commit()
                new_user_id = cur.lastrowid
                cur.close()
            except sqlite3.Error as e:
                if str(e).find('users.username') != -1:
                    errors.append('نام کاربری قبلا ثبت شده‌است')
                else:
                    errors.append('ایمیل قبلا ثبت شده‌است')

                if avatar:
                    if path.exists(avatar):
                        remove(avatar)

        if len(errors):
            self.render('signup.html', name=name, username=username, email=email, errors=errors)
        else:
            self.set_secure_cookie('id', str(new_user_id))
            self.set_secure_cookie('username', username)
            self.redirect('/@' + username)


class LogoutHandler(tornado.web.RequestHandler):
    def get(self):
        self.clear_cookie('id')
        self.clear_cookie('username')
        self.redirect('/login')


class NewTweetHandler(tornado.web.RequestHandler):
    def get(self):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        self.render('new.html', hashtags='', content='', errors=[], **get_user_info(user_id))

    def post(self):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        errors = []

        content = self.get_body_argument('content', default=None)
        hashtags = self.get_body_argument('hashtags', default=None)
        img = self.request.files.get('img', None)

        if not content or not len(content):
            errors.append('توییت نباید خالی باشد')
            content = ''

        splited_hashtags = None
        if content and len(content):
            splited_hashtags = [splited_hashtag.strip() for splited_hashtag in hashtags.split(',')]
            if len(splited_hashtags) > 5:
                errors.append('هشتگ‌های نباید بیشتر از ۵ تا باشند')

        if img:
            upload_file = self.request.files['img'][0]
            img_path = path.join(path.dirname(__file__), 'uploads/tweet-images') + '/' + user_id.decode('utf8') + generateRandomString(10) + \
                          upload_file['filename']
            with open(img_path, 'wb') as file:
                file.write(upload_file['body'])
            img = img_path

        if not len(errors):
            cur = self.application.db.cursor()

            cur.execute(
                'INSERT INTO tweets(userId, content, image) VALUES (?, ?, ?)',
                (int(user_id), content, img,),
            )

            tweet_id = cur.lastrowid

            if len(splited_hashtags):
                self.application.db.executemany(
                    'INSERT OR IGNORE INTO hashtags(hashtag) VALUES (?)',
                    [(hashtag,) for hashtag in splited_hashtags]
                )

            cur.execute(
                'SELECT id FROM hashtags WHERE hashtag in (%s)' % ', '.join('?' * len(splited_hashtags)),
                splited_hashtags
            )
            hashtag_ids = [row[0] for row in cur.fetchall()]

            cur.executemany(
                'INSERT INTO tweetsHashtags(tweetId, hashtagId) VALUES (?, ?)',
                [(tweet_id, hashtag_id) for hashtag_id in hashtag_ids]
            )
            cur.close()

            self.application.db.commit()
        if len(errors):
            self.render('new.html',
                        hashtags=hashtags,
                        content=content,
                        errors=errors,
                        **get_user_info(user_id)
            )
        else:
            self.redirect('/@' + self.get_secure_cookie('username').decode('utf8'))


class AvatarHandler(tornado.web.RequestHandler):
    def get(self, user_id):
        cur = self.application.db.cursor()
        cur.execute('SELECT avatar FROM users WHERE id=?', (int(user_id),))
        avatar_path = cur.fetchone()
        cur.close()

        if not avatar_path or not avatar_path[0] or not path.exists(avatar_path[0]):
            avatar_path = path.join(path.dirname(__file__), 'static/images/avatar.png')
        else:
            avatar_path = avatar_path[0]

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % ntpath.basename(avatar_path))

        with open(avatar_path, 'rb') as f:
            data = f.read()
            self.write(data)
        self.finish()


class FollowersPageHandler(tornado.web.RequestHandler):
    def get(self, username):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        cur = self.application.db.cursor()
        cur.execute('''
            SELECT
              U1.id,
              U1.username,
              U1.name,
              EXISTS(SELECT *
                     FROM usersFriends
                     WHERE usersFriends.userId = ? AND usersFriends.friendId = U1.id) As 'isFollowed'
            FROM usersFriends
              JOIN users AS U1 ON U1.id = usersFriends.userId
              JOIN users AS U2 ON U2.id = usersFriends.friendId
            WHERE U2.username = ?;
        ''', (int(user_id), username,))
        followers = cur.fetchall()
        cur.close()

        self.render('followers-page.html', page_username=username, followers=followers, **get_user_info(user_id))


class FollowingsPageHandler(tornado.web.RequestHandler):
    def get(self, username):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        cur = self.application.db.cursor()
        cur.execute('''
            SELECT
              U2.id,
              U2.username,
              U2.name,
              EXISTS(SELECT *
                     FROM usersFriends
                     WHERE usersFriends.userId = ? AND usersFriends.friendId = U2.id) As 'isFollowed'
            FROM usersFriends
              JOIN users AS U1 ON U1.id = usersFriends.userId
              JOIN users AS U2 ON U2.id = usersFriends.friendId
            WHERE U1.username = ?;
        ''', (int(user_id), username,))
        followings = cur.fetchall()
        cur.close()

        self.render('followings-page.html', page_username=username, followings=followings, **get_user_info(user_id))


class ProfilePageHandler(tornado.web.RequestHandler):
    def get(self, *, page_username):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        if not page_username:
            page_username = self.get_secure_cookie('username').decode('utf8')

        user_info = get_user_info(user_id)

        cur = self.application.db.cursor()

        cur.execute(
            '''
            SELECT
              t.id,
              t.content,
              t.image,
              group_concat(h.hashtag)                                             AS 'hashtags',
              (SELECT count(*)
               FROM tweetsLikes
               WHERE tweetsLikes.tweetId = t.id)                                  AS 'likesCount',
              EXISTS(SELECT *
                     FROM tweetsLikes
                     WHERE tweetsLikes.tweetId = t.id AND tweetsLikes.userId = ?) AS 'isLiked'
            FROM tweetsHashtags AS th
              LEFT JOIN tweets AS t ON t.id = th.tweetId
              JOIN users AS u ON t.userId = u.id
              JOIN hashtags AS h ON h.id = th.hashtagId
            WHERE u.username = ?
            GROUP BY t.id
            ORDER BY t.id
              DESC;
          ''',
            (int(user_id), page_username,)
        )

        tweets = [
            (tweet[0],
             tweet[1],
             tweet[2],
             (tweet[3].split(',') if tweet[3] != '' else None),
             tweet[4],
             tweet[5]
             )

            for tweet in cur.fetchall()
        ]

        non_login_user_info = dict()
        if user_info['username'] != page_username:
            non_login_user_info = get_user_info(page_username, by='username', pre='non_login_')

            if not non_login_user_info:
                raise tornado.web.HTTPError(404)

            cur.execute(
                'SELECT count(*) FROM usersFriends WHERE userId=? AND friendId=?',
                (int(user_id), int(non_login_user_info['non_login_user_id']))
            )
            non_login_user_info['non_login_isFollowed'] = cur.fetchone()[0] == 1

            cur.execute(
                'SELECT count(*) FROM usersFriends WHERE friendId=? AND userId=?',
                (int(user_id), int(non_login_user_info['non_login_user_id']))
            )
            non_login_user_info['non_login_isFollowing'] = cur.fetchone()[0] == 1

        cur.close()

        self.render('profile.html', page_username=page_username, tweets=tweets, **user_info, **non_login_user_info)


class FollowRequestHandler(tornado.web.RequestHandler):
    def post(self, friend_id):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.send_error(403)

        cur = self.application.db.cursor()
        cur.execute('INSERT OR IGNORE INTO usersFriends (userId, friendId) VALUES (?, ?);', (int(user_id), int(friend_id)))
        cur.connection.commit()
        cur.close()

        self.set_status(201)


class UnfollowRequestHandler(tornado.web.RequestHandler):
    def post(self, friend_id):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.send_error(403)

        cur = self.application.db.cursor()
        cur.execute('DELETE FROM usersFriends WHERE userId=? AND friendId=?', (int(user_id), int(friend_id)))
        cur.connection.commit()
        cur.close()

        self.set_status(200)


class TweetImageHandler(tornado.web.RequestHandler):
    def get(self, tweet_id):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.set_status(404)

        cur = self.application.db.cursor()
        cur.execute('SELECT image FROM tweets WHERE id=?', (int(tweet_id),))
        image_path = cur.fetchone()
        cur.close()

        if not image_path or not image_path[0] or not path.exists(image_path[0]):
            return self.set_status(404)
        else:
            image_path = image_path[0]

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % ntpath.basename(image_path))

        with open(image_path, 'rb') as f:
            data = f.read()
            self.write(data)
        self.finish()


class TweetLikeHandler(tornado.web.RequestHandler):
    def post(self, tweet_id):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.send_error(403)

        cur = self.application.db.cursor()

        # check if tweet exists
        cur.execute('SELECT id FROM tweets WHERE id=?', (int(tweet_id),))
        if not cur.fetchone():
            return self.send_error(404)

        cur.execute('INSERT OR IGNORE INTO tweetsLikes (tweetId, userId) VALUES (?, ?);', (int(tweet_id), int(user_id)))
        cur.connection.commit()
        cur.close()

        self.set_status(201)


class TweetUnlikeHandler(tornado.web.RequestHandler):
    def post(self, tweet_id):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.send_error(403)

        cur = self.application.db.cursor()

        cur.execute('DELETE FROM tweetsLikes WHERE tweetId=? AND userId=?', (int(tweet_id), int(user_id)))
        cur.connection.commit()
        cur.close()

        self.set_status(200)


class FeedPageHandler(tornado.web.RequestHandler):
    def get(self):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        user_info = get_user_info(user_id)

        cur = self.application.db.cursor()

        cur.execute(
            '''
            SELECT
              t.id,
              t.content,
              t.image,
              group_concat(h.hashtag)                                             AS 'hashtags',
              (SELECT count(*)
               FROM tweetsLikes
               WHERE tweetsLikes.tweetId = t.id)                                  AS 'likesCount',
              EXISTS(SELECT *
                     FROM tweetsLikes
                     WHERE tweetsLikes.tweetId = t.id AND tweetsLikes.userId = ?) AS 'isLiked',
              u.id                                                                AS 'userId',
              u.username                                                          AS 'userUsername',
              u.name                                                              AS 'userName'
            FROM tweetsHashtags AS th
              LEFT JOIN tweets AS t ON t.id = th.tweetId
              JOIN users AS u ON t.userId = u.id
              JOIN hashtags AS h ON h.id = th.hashtagId
            WHERE EXISTS(
                      SELECT *
                      FROM usersFriends AS uf
                      WHERE uf.userId = ?
                            AND uf.friendId = u.id
                  ) OR u.id = ?
            GROUP BY t.id
            ORDER BY t.id
              DESC;
          ''',
            (int(user_id), int(user_id), int(user_id))
        )

        tweets = [
            (tweet[0],
             tweet[1],
             tweet[2],
             (tweet[3].split(',') if tweet[3] != '' else None),
             tweet[4],
             tweet[5],
             tweet[6],
             tweet[7],
             tweet[8]
             )

            for tweet in cur.fetchall()
        ]
        cur.close()

        self.render('feed.html', tweets=tweets, **user_info)


class HashtagPageHandler(tornado.web.RequestHandler):
    def get(self, hashtag):
        user_id = self.get_secure_cookie('id')

        if not user_id:
            return self.redirect('/login')

        user_info = get_user_info(user_id)

        cur = self.application.db.cursor()

        cur.execute(
            '''
            SELECT
              t.id,
              t.content,
              t.image,
              group_concat(h.hashtag)                                             AS 'hashtags',
              (SELECT count(*)
               FROM tweetsLikes
               WHERE tweetsLikes.tweetId = t.id)                                  AS 'likesCount',
              EXISTS(SELECT *
                     FROM tweetsLikes
                     WHERE tweetsLikes.tweetId = t.id AND tweetsLikes.userId = ?) AS 'isLiked',
              u.id                                                                AS 'userId',
              u.username                                                          AS 'userUsername',
              u.name                                                              AS 'userName'
            FROM tweetsHashtags AS th
              LEFT JOIN tweets AS t ON t.id = th.tweetId
              JOIN users AS u ON t.userId = u.id
              JOIN hashtags AS h ON h.id = th.hashtagId
            WHERE EXISTS(
                SELECT *
                FROM tweetsHashtags AS th2
                  JOIN hashtags as h2 ON h2.id = th2.hashtagId
                WHERE h2.hashtag = ?
                      AND th2.tweetId = t.id
            )
            GROUP BY t.id
            ORDER BY t.id
              DESC;
          ''',
            (int(user_id), hashtag)
        )

        tweets = [
            (tweet[0],
             tweet[1],
             tweet[2],
             (tweet[3].split(',') if tweet[3] != '' else None),
             tweet[4],
             tweet[5],
             tweet[6],
             tweet[7],
             tweet[8]
             )

            for tweet in cur.fetchall()
        ]
        cur.close()

        self.render('hashtag.html', hashtag=hashtag, tweets=tweets, **user_info)


def make_app():
    return tornado.web.Application(
        [
            (r"/login/?", LoginHandler),
            (r"/signup/?", SignupHandler),
            (r"/logout/?", LogoutHandler),
            (r"/new/?", NewTweetHandler),
            (r"/avatar/([0-9]+)/?", AvatarHandler),
            (r"/@(.+)/followers/?", FollowersPageHandler),
            (r"/@(.+)/followings/?", FollowingsPageHandler),
            (r"/(?:@(?P<page_username>.+))?/?", ProfilePageHandler),
            (r"/users/id/([0-9]+)/follow/?", FollowRequestHandler),
            (r"/users/id/([0-9]+)/un-follow/?", UnfollowRequestHandler),
            (r"/tweets/id/([0-9]+)/image/?", TweetImageHandler),
            (r"/tweets/id/([0-9]+)/like/?", TweetLikeHandler),
            (r"/tweets/id/([0-9]+)/unlike/?", TweetUnlikeHandler),
            (r"/feed/?", FeedPageHandler),
            (r"/tweets/hashtag/(.+)/?", HashtagPageHandler),
        ],

        static_path=path.join(path.dirname(__file__), 'static'),
        template_path=path.join(path.dirname(__file__), 'templates'),
        cookie_secret=generateRandomString(50),
    )


if __name__ == "__main__":
    app = make_app()
    db = app.db = sqlite3.connect("db.sqlite")
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
