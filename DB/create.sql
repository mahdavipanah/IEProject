create table hashtags
(
  id      INTEGER PRIMARY KEY AUTOINCREMENT not null,
  hashtag TEXT    not null
);

create unique index hashtags_hashtag_uindex
  on hashtags (hashtag);

create unique index hashtags_id_uindex
  on hashtags (id);
-- ------------------------------------------------------------------------
create table tweetsHashtags
(
  tweetId   INTEGER not null,
  hashtagId INTEGER not null,
  constraint tweetsHashtags_tweetId_hashtagId_pk
  primary key (tweetId, hashtagId)
);
-- ------------------------------------------------------------------------
create table users
(
  id       INTEGER PRIMARY KEY AUTOINCREMENT not null,
  name     TEXT not null,
  username int     not null,
  email    int     not null,
  password TEXT not null,
  avatar   TEXT default NULL
);

create unique index users_email_uindex
  on users (email);

create unique index users_id_uindex
  on users (id);

create unique index users_username_uindex
  on users (username);
-- ------------------------------------------------------------------------
create table tweets
(
  id      INTEGER PRIMARY KEY AUTOINCREMENT not null,
  userId  INTEGER not null
    constraint tweets_users_id_fk
    references users(id)
      on update cascade
      on delete cascade,
  content TEXT    not null,
  image   TEXT default NULL
);

create unique index tweets_id_uindex
  on tweets (id);
-- ------------------------------------------------------------------------
create table tweetsLikes
(
  tweetId   INTEGER not null
    constraint tweetsLikes_tweets_id_fk
    references tweets(id)
      on update cascade
      on delete cascade,
  userId    INTEGER not null
    constraint tweetsLikes_users_id_fk
    references users(id)
      on update cascade
      on delete cascade,
  constraint tweetsLikes_tweetId_userId_pk
  primary key (tweetId, userId)
);
-- ------------------------------------------------------------------------
create table usersFriends
(
  userId    INTEGER not null
    constraint usersFriends_users_id_fk
    references users(id)
      on update cascade
      on delete cascade,
  friendId  INTEGER not null
    constraint usersFriends_users_id_fk_2
    references users(id)
      on update cascade
      on delete cascade,
  constraint usersFriends_userId_friendId_pk
  primary key (userId, friendId)
);

create unique index usersFriends_friendId_userId_uindex
  on usersFriends (friendId, userId);
